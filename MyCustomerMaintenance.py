
from dataclasses import dataclass
        
import datetime
from operator import contains, iconcat
from tkinter import *
import sqlite3
import tkinter.ttk as ttk
import tkinter.messagebox as tkMessageBox

@dataclass
class Customer :
    Lastname : str
    Middlename : str
    Firstname : str
    Suffix : str
    Address1 : str
    Address2 : str
    Telephone : str
    Zipcode : str
    State : str 
    Country : str 
    Areacode : str
    Birthdate : datetime.date
    Age : float 
    Gender : str
    UserRoleCode : str 
    ICNumber : str
class UserTable :
    UserTableUserRoleCode : str
    UserTableUserID : str 
    UserTableCustomercodeclassification : str 
    UserTableCompanycodeclassification : str 
    UserTableCountrycoddeclassification : str  
    UserTableFirstName : str
    UserTableLastName : str 
    UserTableMiddleName : str  
    UserTableSuffix : str 
    UserTableCompanyCode : str

#Currency All, USD, PHP
#Product All, Loans, Syndication
#Company All, Airbus
#Country All, Singapore, France
class UserMatrix :
    UserMatrixUserID : str 
    UserMatrixCurrencycodeclassification : str 
    UserMatrixProductcodeclassification : str = ["All", "Loans", "Syndication"]
    UserMatrixCompanycodeclassification : str 
    UserMatrixCountrycoddeclassification : str  
    UserMatrixBranchcodeclassification : str 
    UserMatrixSubbranchcodeclassification : str  
class Company : 
    CompanyCode : str 
    CompanyName : str 
    CompanyAddress1 : str
    CompanyAddress2 : str
    CompanyState : str
    CompanyZipcode : str
    CompanyTelephone : str
    CompanyContact1FirstName  : str
    CompanyContact1MiddleName : str
    CompanyContact1LastName : str
    CompanyContact1Suffix : str
    CompanyDomicileCountry : str
    CompanyDomicileState : str
    CompanyDomicileCountryCode : str
    CompanyDomicileTelephone : str
    CompanySECRegistration : str 
    CompanyDateofIncorporation : datetime.date
    CompanyCreditRatingFitch : str 
    CompanyCreditRatingOthers : str 
    CompanyTaxclassification : str 
    CompanyDepartment : str 
    CompanyRelationshipManager : str
#aggregate facilities
class FacilitySetup :
    CompanyCode : str 
    FacilityNumber : str 
    FacilityAmount : float
    ProductCode : str 
    CurrencyCode : str
    Currency2nd : str 
    Currency3rd : str 
    FacilityAggregate : str 
    Revolver : str 
    FacilityTotalAmountAggregate : float
    FacilityRemainingAggregate : float 
    FacilityExpirationDate : datetime
    Facilitygraceperiod : float
#transactional facilities
class TransactionTable :
    TransactionCode : str
    ProductCode : str 
    CurrencyCode : str
    TransactionAmount : float
    FacilityNumber : str 
    AccountsPostDate = datetime.date
    AccountsValueDate = datetime.date
    AccountsAvailableDate = datetime.date
    Interestrate : float 
    EndDate : datetime.date
    BuyCCY : float
    BuyAmount : float
    SellCCY : float 
    SellAmount : float 
    FXrate : float 
    swappoints : float 
    BuyCCYIntrate : float
    SellCCYIntrate : float 
    Subbranch : str 
    Branch  :  str 
    Department : str 
    IncomeDept : str 
    BookingDept : str 
    BasisCalculation : str 
    FirstInterestDate : datetime.date
    FirstAccrualShort : str 
    FirstAccrualLong : str 
    TotalNumberofdays : float 
    CouponDateStart : datetime.date
    CouponDateEnd : datetime.date
class ProductTable : 
    ProductCode : str 
    CurrencyCode : str    
    ProductDescription : str 
    Interestrate : float 
    EndDate : datetime.date
    BuyCCY : float
    BuyAmount : float
    SellCCY : float 
    SellAmount : float 
    FXrate : float 
    swappoints : float 
    BuyCCYIntrate : float
    SellCCYIntrate : float 
    BasisCalculation : str 
    FirstInterestDate : datetime.date
    FirstAccrualShort : str 
    FirstAccrualLong : str 
    TotalNumberofdays : float 
    CouponDateStart : datetime.date
    CouponDateEnd : datetime.date

class BalanceTable:
    BalanceSheetSubitem = str 
    BalanceSheetItem = str 
    AccountsID = str
    AccountsType = str 
    AccountsCCY = str
    AccountsRef = str 
    AccountsBranch = str 
    AccountsSubbranch = str 
    AccountsDept = str 
    BalanceAvailableAmount = float 
    BalancePostAmount = float 
    BalanceValueAmount = float 
    AccountsPostDate = datetime.date
    AccountsValueDate = datetime.date
    AccountsAvailableDate = datetime.date
    BanksAcct = str 
class AccountsTable : 
    BalanceSheetSubitem = str 
    BalanceSheetItem = str 
    AccountsID = str
    AccountsType = str 
    AccountsCCY = str
    AccountsRef = str 
    AccountsBranch = str 
    AccountsSubbranch = str 
    AccountsDept = str 
    BanksAcct = str 
    
class JournalTable :
    BalanceSheetSubitem = str 
    BalanceSheetItem = str 
    AccountsID = str
    AccountsType = str 
    AccountsCCY = str
    AccountsRef = str 
    AccountsBranch = str 
    AccountsSubbranch = str 
    AccountsDept = str 
    JournalAmount = float 
    TotalAmount = float 
    AccountsPostDate = datetime.date
    AccountsValueDate = datetime.date
    AccountsAvailableDate = datetime.date
    BanksAcct = str 
    DRCR = str 
    TransactionCode : str
# ========================================FUNCTIONS ==============================================
#Create some instances
#MyCustomerrecord = Customer ()

def assigncustomer():
    MyCustomerrecord = Customer ("Ibay"
        ,"Diestro"
        ,"Jose"
        ,"Jr"
        ,"13 Holland Drive"
        ,"Unit #10-40"
        ,"94747674"
        ,"271013"
        ,"Singapore"
        ,"Singapore"
        ,"65"
        ,"19740706"
        ,47
        ,"Male"
        ,"Lending01"
        ,"S7484717Z")
    #Dictionary of currency codes
  
    ccydescription = {
        'AFN' : 'Afghani',
        'EUR' : 'Euro',
        'ALL' : 'Lek',
        'DZD' : 'Algerian Dinar',
        'USD' : 'US Dollar',
        'EUR' : 'Euro',
        'AOA' : 'Kwanza',
        'XCD' : 'East Caribbean Dollar',
        'XCD' : 'East Caribbean Dollar',
        'ARS' : 'Argentine Peso',
        'AMD' : 'Armenian Dram',
        'AWG' : 'Aruban Florin',
        'AUD' : 'Australian Dollar',
        'EUR' : 'Euro',
        'AZN' : 'Azerbaijanian Manat',
        'BSD' : 'Bahamian Dollar',
        'BHD' : 'Bahraini Dinar',
        'BDT' : 'Taka',
        'BBD' : 'Barbados Dollar',
        'BYR' : 'Belarussian Ruble',
        'EUR' : 'Euro',
        'BZD' : 'Belize Dollar',
        'XOF' : 'CFA Franc BCEAO',
        'BMD' : 'Bermudian Dollar',
        'BTN' : 'Ngultrum',
        'INR' : 'Indian Rupee',
        'BOB' : 'Boliviano',
        'BOV' : 'Mvdol',
        'USD' : 'US Dollar',
        'BAM' : 'Convertible Mark',
        'BWP' : 'Pula',
        'NOK' : 'Norwegian Krone',
        'BRL' : 'Brazilian Real',
        'USD' : 'US Dollar',
        'BND' : 'Brunei Dollar',
        'BGN' : 'Bulgarian Lev',
        'XOF' : 'CFA Franc BCEAO',
        'BIF' : 'Burundi Franc',
        'KHR' : 'Riel',
        'XAF' : 'CFA Franc BEAC',
        'CAD' : 'Canadian Dollar',
        'CVE' : 'Cabo Verde Escudo',
        'KYD' : 'Cayman Islands Dollar',
        'XAF' : 'CFA Franc BEAC',
        'XAF' : 'CFA Franc BEAC',
        'CLF' : 'Unidad de Fomento',
        'CLP' : 'Chilean Peso',
        'CNY' : 'Yuan Renminbi',
        'AUD' : 'Australian Dollar',
        'AUD' : 'Australian Dollar',
        'COP' : 'Colombian Peso',
        'COU' : 'Unidad de Valor Real',
        'KMF' : 'Comoro Franc',
        'XAF' : 'CFA Franc BEAC',
        'CDF' : 'Congolese Franc',
        'NZD' : 'New Zealand Dollar',
        'CRC' : 'Costa Rican Colon',
        'XOF' : 'CFA Franc BCEAO',
        'HRK' : 'Croatian Kuna',
        'CUC' : 'Peso Convertible',
        'CUP' : 'Cuban Peso',
        'ANG' : 'Netherlands Antillean Guilder',
        'EUR' : 'Euro',
        'CZK' : 'Czech Koruna',
        'DKK' : 'Danish Krone',
        'DJF' : 'Djibouti Franc',
        'XCD' : 'East Caribbean Dollar',
        'DOP' : 'Dominican Peso',
        'USD' : 'US Dollar',
        'EGP' : 'Egyptian Pound',
        'SVC' : 'El Salvador Colon',
        'USD' : 'US Dollar',
        'XAF' : 'CFA Franc BEAC',
        'ERN' : 'Nakfa',
        'EUR' : 'Euro',
        'ETB' : 'Ethiopian Birr',
        'EUR' : 'Euro',
        'FKP' : 'Falkland Islands Pound',
        'DKK' : 'Danish Krone',
        'FJD' : 'Fiji Dollar',
        'EUR' : 'Euro',
        'EUR' : 'Euro',
        'EUR' : 'Euro',
        'XPF' : 'CFP Franc',
        'EUR' : 'Euro',
        'XAF' : 'CFA Franc BEAC',
        'GMD' : 'Dalasi',
        'GEL' : 'Lari',
        'EUR' : 'Euro',
        'GHS' : 'Ghana Cedi',
        'GIP' : 'Gibraltar Pound',
        'EUR' : 'Euro',
        'DKK' : 'Danish Krone',
        'XCD' : 'East Caribbean Dollar',
        'EUR' : 'Euro',
        'USD' : 'US Dollar',
        'GTQ' : 'Quetzal',
        'GBP' : 'Pound Sterling',
        'GNF' : 'Guinea Franc',
        'XOF' : 'CFA Franc BCEAO',
        'GYD' : 'Guyana Dollar',
        'HTG' : 'Gourde',
        'USD' : 'US Dollar',
        'AUD' : 'Australian Dollar',
        'EUR' : 'Euro',
        'HNL' : 'Lempira',
        'HKD' : 'Hong Kong Dollar',
        'HUF' : 'Forint',
        'ISK' : 'Iceland Krona',
        'INR' : 'Indian Rupee',
        'IDR' : 'Rupiah',
        'XDR' : 'SDR (Special Drawing Right)',
        'IRR' : 'Iranian Rial',
        'IQD' : 'Iraqi Dinar',
        'EUR' : 'Euro',
        'GBP' : 'Pound Sterling',
        'ILS' : 'New Israeli Sheqel',
        'EUR' : 'Euro',
        'JMD' : 'Jamaican Dollar',
        'JPY' : 'Yen',
        'GBP' : 'Pound Sterling',
        'JOD' : 'Jordanian Dinar',
        'KZT' : 'Tenge',
        'KES' : 'Kenyan Shilling',
        'AUD' : 'Australian Dollar',
        'KPW' : 'North Korean Won',
        'KRW' : 'Won',
        'KWD' : 'Kuwaiti Dinar',
        'KGS' : 'Som',
        'LAK' : 'Kip',
        'EUR' : 'Euro',
        'LBP' : 'Lebanese Pound',
        'LSL' : 'Loti',
        'ZAR' : 'Rand',
        'LRD' : 'Liberian Dollar',
        'LYD' : 'Libyan Dinar',
        'CHF' : 'Swiss Franc',
        'EUR' : 'Euro',
        'EUR' : 'Euro',
        'MOP' : 'Pataca',
        'MKD' : 'Denar',
        'MGA' : 'Malagasy Ariary',
        'MWK' : 'Kwacha',
        'MYR' : 'Malaysian Ringgit',
        'MVR' : 'Rufiyaa',
        'XOF' : 'CFA Franc BCEAO',
        'EUR' : 'Euro',
        'USD' : 'US Dollar',
        'EUR' : 'Euro',
        'MRO' : 'Ouguiya',
        'MUR' : 'Mauritius Rupee',
        'EUR' : 'Euro',
        'XUA' : 'ADB Unit of Account',
        'MXN' : 'Mexican Peso',
        'MXV' : 'Mexican Unidad de Inversion (UDI)',
        'USD' : 'US Dollar',
        'MDL' : 'Moldovan Leu',
        'EUR' : 'Euro',
        'MNT' : 'Tugrik',
        'EUR' : 'Euro',
        'XCD' : 'East Caribbean Dollar',
        'MAD' : 'Moroccan Dirham',
        'MZN' : 'Mozambique Metical',
        'MMK' : 'Kyat',
        'NAD' : 'Namibia Dollar',
        'ZAR' : 'Rand',
        'AUD' : 'Australian Dollar',
        'NPR' : 'Nepalese Rupee',
        'EUR' : 'Euro',
        'XPF' : 'CFP Franc',
        'NZD' : 'New Zealand Dollar',
        'NIO' : 'Cordoba Oro',
        'XOF' : 'CFA Franc BCEAO',
        'NGN' : 'Naira',
        'NZD' : 'New Zealand Dollar',
        'AUD' : 'Australian Dollar',
        'USD' : 'US Dollar',
        'NOK' : 'Norwegian Krone',
        'OMR' : 'Rial Omani',
        'PKR' : 'Pakistan Rupee',
        'USD' : 'US Dollar',
        'PAB' : 'Balboa',
        'USD' : 'US Dollar',
        'PGK' : 'Kina',
        'PYG' : 'Guarani',
        'PEN' : 'Nuevo Sol',
        'PHP' : 'Philippine Peso',
        'NZD' : 'New Zealand Dollar',
        'PLN' : 'Zloty',
        'EUR' : 'Euro',
        'USD' : 'US Dollar',
        'QAR' : 'Qatari Rial',
        'EUR' : 'Euro',
        'RON' : 'New Romanian Leu',
        'RUB' : 'Russian Ruble',
        'RWF' : 'Rwanda Franc',
        'EUR' : 'Euro',
        'SHP' : 'Saint Helena Pound',
        'XCD' : 'East Caribbean Dollar',
        'XCD' : 'East Caribbean Dollar',
        'EUR' : 'Euro',
        'EUR' : 'Euro',
        'XCD' : 'East Caribbean Dollar',
        'WST' : 'Tala',
        'EUR' : 'Euro',
        'STD' : 'Dobra',
        'SAR' : 'Saudi Riyal',
        'XOF' : 'CFA Franc BCEAO',
        'RSD' : 'Serbian Dinar',
        'SCR' : 'Seychelles Rupee',
        'SLL' : 'Leone',
        'SGD' : 'Singapore Dollar',
        'ANG' : 'Netherlands Antillean Guilder',
        'EUR' : 'Euro',
        'EUR' : 'Euro',
        'SBD' : 'Solomon Islands Dollar',
        'SOS' : 'Somali Shilling',
        'ZAR' : 'Rand',
        'SSP' : 'South Sudanese Pound',
        'EUR' : 'Euro',
        'LKR' : 'Sri Lanka Rupee',
        'SDG' : 'Sudanese Pound',
        'SRD' : 'Surinam Dollar',
        'NOK' : 'Norwegian Krone',
        'SZL' : 'Lilangeni',
        'SEK' : 'Swedish Krona',
        'CHE' : 'WIR Euro',
        'CHF' : 'Swiss Franc',
        'CHW' : 'WIR Franc',
        'SYP' : 'Syrian Pound',
        'TWD' : 'New Taiwan Dollar',
        'TJS' : 'Somoni',
        'TZS' : 'Tanzanian Shilling',
        'THB' : 'Baht',
        'USD' : 'US Dollar',
        'XOF' : 'CFA Franc BCEAO',
        'NZD' : 'New Zealand Dollar',
        'TOP' : 'Pa’anga',
        'TTD' : 'Trinidad and Tobago Dollar',
        'TND' : 'Tunisian Dinar',
        'TRY' : 'Turkish Lira',
        'TMT' : 'Turkmenistan New Manat',
        'USD' : 'US Dollar',
        'AUD' : 'Australian Dollar',
        'UGX' : 'Uganda Shilling',
        'UAH' : 'Hryvnia',
        'AED' : 'UAE Dirham',
        'GBP' : 'Pound Sterling',
        'USD' : 'US Dollar',
        'USN' : 'US Dollar (Next day)',
        'USD' : 'US Dollar',
        'UYI' : 'Uruguay Peso en Unidades Indexadas (URUIURUI)',
        'UYU' : 'Peso Uruguayo',
        'UZS' : 'Uzbekistan Sum',
        'VUV' : 'Vatu',
        'VEF' : 'Bolivar',
        'VND' : 'Dong',
        'USD' : 'US Dollar',
        'USD' : 'US Dollar',
        'XPF' : 'CFP Franc',
        'MAD' : 'Moroccan Dirham',
        'YER' : 'Yemeni Rial',
        'ZMW' : 'Zambian Kwacha',
        'ZWL' : 'Zimbabwe Dollar',
        'XBA' : 'Bond Markets Unit European Composite Unit (EURCO)',
        'XBB' : 'Bond Markets Unit European Monetary Unit (E.M.U.-6)',
        'XBC' : 'Bond Markets Unit European Unit of Account 9 (E.U.A.-9)',
        'XBD' : 'Bond Markets Unit European Unit of Account 17 (E.U.A.-17)',
        'XTS' : 'Codes specifically reserved for testing purposes',
        'XXX' : 'The codes assigned for transactions where no currency is involved',
        'XAU' : 'Gold',
        'XPD' : 'Palladium',
        'XPT' : 'Platinum',
        'XAG' : 'Silver'
    }

    countrydescription = {
        'AF' : 'Afghanistan',
        'AL' : 'Albania',
        'DZ' : 'Algeria',
        'AS' : 'American Samoa',
        'AD' : 'Andorra',
        'AO' : 'Angola',
        'AI' : 'Anguilla',
        'AQ' : 'Antarctica',
        'AG' : 'Antigua and Barbuda',
        'AR' : 'Argentina',
        'AM' : 'Armenia',
        'AW' : 'Aruba',
        'AU' : 'Australia',
        'AT' : 'Austria',
        'AZ' : 'Azerbaijan',
        'BS' : 'Bahamas (the)',
        'BH' : 'Bahrain',
        'BD' : 'Bangladesh',
        'BB' : 'Barbados',
        'BY' : 'Belarus',
        'BE' : 'Belgium',
        'BZ' : 'Belize',
        'BJ' : 'Benin',
        'BM' : 'Bermuda',
        'BT' : 'Bhutan',
        'BO' : 'Bolivia (Plurinational State of)',
        'BQ' : 'Bonaire, Sint Eustatius and Saba',
        'BA' : 'Bosnia and Herzegovina',
        'BW' : 'Botswana',
        'BV' : 'Bouvet Island',
        'BR' : 'Brazil',
        'IO' : 'British Indian Ocean Territory (the)',
        'BN' : 'Brunei Darussalam',
        'BG' : 'Bulgaria',
        'BF' : 'Burkina Faso',
        'BI' : 'Burundi',
        'CV' : 'Cabo Verde',
        'KH' : 'Cambodia',
        'CM' : 'Cameroon',
        'CA' : 'Canada',
        'KY' : 'Cayman Islands (the)',
        'CF' : 'Central African Republic (the)',
        'TD' : 'Chad',
        'CL' : 'Chile',
        'CN' : 'China',
        'CX' : 'Christmas Island',
        'CC' : 'Cocos (Keeling) Islands (the)',
        'CO' : 'Colombia',
        'KM' : 'Comoros (the)',
        'CD' : 'Congo (the Democratic Republic of the)',
        'CG' : 'Congo (the)',
        'CK' : 'Cook Islands (the)',
        'CR' : 'Costa Rica',
        'HR' : 'Croatia',
        'CU' : 'Cuba',
        'CW' : 'Curaçao',
        'CY' : 'Cyprus',
        'CZ' : 'Czechia',
        'CI' : "Côte d'Ivoire",
        'DK' : 'Denmark',
        'DJ' : 'Djibouti',
        'DM' : 'Dominica',
        'DO' : 'Dominican Republic (the)',
        'EC' : 'Ecuador',
        'EG' : 'Egypt',
        'SV' : 'El Salvador',
        'GQ' : 'Equatorial Guinea',
        'ER' : 'Eritrea',
        'EE' : 'Estonia',
        'SZ' : 'Eswatini',
        'ET' : 'Ethiopia',
        'FK' : 'Falkland Islands (the) [Malvinas]',
        'FO' : 'Faroe Islands (the)',
        'FJ' : 'Fiji',
        'FI' : 'Finland',
        'FR' : 'France',
        'GF' : 'French Guiana',
        'PF' : 'French Polynesia',
        'TF' : 'French Southern Territories (the)',
        'GA' : 'Gabon',
        'GM' : 'Gambia (the)',
        'GE' : 'Georgia',
        'DE' : 'Germany',
        'GH' : 'Ghana',
        'GI' : 'Gibraltar',
        'GR' : 'Greece',
        'GL' : 'Greenland',
        'GD' : 'Grenada',
        'GP' : 'Guadeloupe',
        'GU' : 'Guam',
        'GT' : 'Guatemala',
        'GG' : 'Guernsey',
        'GN' : 'Guinea',
        'GW' : 'Guinea-Bissau',
        'GY' : 'Guyana',
        'HT' : 'Haiti',
        'HM' : 'Heard Island and McDonald Islands',
        'VA' : 'Holy See (the)',
        'HN' : 'Honduras',
        'HK' : 'Hong Kong',
        'HU' : 'Hungary',
        'IS' : 'Iceland',
        'IN' : 'India',
        'ID' : 'Indonesia',
        'IR' : 'Iran (Islamic Republic of)',
        'IQ' : 'Iraq',
        'IE' : 'Ireland',
        'IM' : 'Isle of Man',
        'IL' : 'Israel',
        'IT' : 'Italy',
        'JM' : 'Jamaica',
        'JP' : 'Japan',
        'JE' : 'Jersey',
        'JO' : 'Jordan',
        'KZ' : 'Kazakhstan',
        'KE' : 'Kenya',
        'KI' : 'Kiribati',
        'KP' : "Korea (the Democratic People's Republic of)" ,
        'KR' : 'Korea (the Republic of)',
        'KW' : 'Kuwait',
        'KG' : 'Kyrgyzstan',
        'LA' : "Lao People's Democratic Republic (the)",
        'LV' : 'Latvia',
        'LB' : 'Lebanon',
        'LS' : 'Lesotho',
        'LR' : 'Liberia',
        'LY' : 'Libya',
        'LI' : 'Liechtenstein',
        'LT' : 'Lithuania',
        'LU' : 'Luxembourg',
        'MO' : 'Macao',
        'MG' : 'Madagascar',
        'MW' : 'Malawi',
        'MY' : 'Malaysia',
        'MV' : 'Maldives',
        'ML' : 'Mali',
        'MT' : 'Malta',
        'MH' : 'Marshall Islands (the)',
        'MQ' : 'Martinique',
        'MR' : 'Mauritania',
        'MU' : 'Mauritius',
        'YT' : 'Mayotte',
        'MX' : 'Mexico',
        'FM' : 'Micronesia (Federated States of)',
        'MD' : 'Moldova (the Republic of)',
        'MC' : 'Monaco',
        'MN' : 'Mongolia',
        'ME' : 'Montenegro',
        'MS' : 'Montserrat',
        'MA' : 'Morocco',
        'MZ' : 'Mozambique',
        'MM' : 'Myanmar',
        'NA' : 'Namibia',
        'NR' : 'Nauru',
        'NP' : 'Nepal',
        'NL' : 'Netherlands (the)',
        'NC' : 'New Caledonia',
        'NZ' : 'New Zealand',
        'NI' : 'Nicaragua',
        'NE' : 'Niger (the)',
        'NG' : 'Nigeria',
        'NU' : 'Niue',
        'NF' : 'Norfolk Island',
        'MP' : 'Northern Mariana Islands (the)',
        'NO' : 'Norway',
        'OM' : 'Oman',
        'PK' : 'Pakistan',
        'PW' : 'Palau',
        'PS' : 'Palestine, State of',
        'PA' : 'Panama',
        'PG' : 'Papua New Guinea',
        'PY' : 'Paraguay',
        'PE' : 'Peru',
        'PH' : 'Philippines (the)',
        'PN' : 'Pitcairn',
        'PL' : 'Poland',
        'PT' : 'Portugal',
        'PR' : 'Puerto Rico',
        'QA' : 'Qatar',
        'MK' : 'Republic of North Macedonia',
        'RO' : 'Romania',
        'RU' : 'Russian Federation (the)',
        'RW' : 'Rwanda',
        'RE' : 'Réunion',
        'BL' : 'Saint Barthélemy',
        'SH' : 'Saint Helena, Ascension and Tristan da Cunha',
        'KN' : 'Saint Kitts and Nevis',
        'LC' : 'Saint Lucia',
        'MF' : 'Saint Martin (French part)',
        'PM' : 'Saint Pierre and Miquelon',
        'VC' : 'Saint Vincent and the Grenadines',
        'WS' : 'Samoa',
        'SM' : 'San Marino',
        'ST' : 'Sao Tome and Principe',
        'SA' : 'Saudi Arabia',
        'SN' : 'Senegal',
        'RS' : 'Serbia',
        'SC' : 'Seychelles',
        'SL' : 'Sierra Leone',
        'SG' : 'Singapore',
        'SX' : 'Sint Maarten (Dutch part)',
        'SK' : 'Slovakia',
        'SI' : 'Slovenia',
        'SB' : 'Solomon Islands',
        'SO' : 'Somalia',
        'ZA' : 'South Africa',
        'GS' : 'South Georgia and the South Sandwich Islands',
        'SS' : 'South Sudan',
        'ES' : 'Spain',
        'LK' : 'Sri Lanka',
        'SD' : 'Sudan (the)',
        'SR' : 'Suriname',
        'SJ' : 'Svalbard and Jan Mayen',
        'SE' : 'Sweden',
        'CH' : 'Switzerland',
        'SY' : 'Syrian Arab Republic',
        'TW' : 'Taiwan',
        'TJ' : 'Tajikistan',
        'TZ' : 'Tanzania, United Republic of',
        'TH' : 'Thailand',
        'TL' : 'Timor-Leste',
        'TG' : 'Togo',
        'TK' : 'Tokelau',
        'TO' : 'Tonga',
        'TT' : 'Trinidad and Tobago',
        'TN' : 'Tunisia',
        'TR' : 'Turkey',
        'TM' : 'Turkmenistan',
        'TC' : 'Turks and Caicos Islands (the)',
        'TV' : 'Tuvalu',
        'UG' : 'Uganda',
        'UA' : 'Ukraine',
        'AE' : 'United Arab Emirates (the)',
        'GB' : 'United Kingdom of Great Britain and Northern Ireland (the)',
        'UM' : 'United States Minor Outlying Islands (the)',
        'US' : 'United States of America (the)',
        'UY' : 'Uruguay',
        'UZ' : 'Uzbekistan',
        'VU' : 'Vanuatu',
        'VE' : 'Venezuela (Bolivarian Republic of)',
        'VN' : 'Viet Nam',
        'VG' : 'Virgin Islands (British)',
        'VI' : 'Virgin Islands (U.S.)',
        'WF' : 'Wallis and Futuna',
        'EH' : 'Western Sahara',
        'YE' : 'Yemen',
        'ZM' : 'Zambia',
        'ZW' : 'Zimbabwe',
        'AX' : 'Åland Islands'
        }
# end of function


 
#============================METHODS=====================================
 
def Database():
    conn = sqlite3.connect("pythontut.db")
    cursor = conn.cursor()
    cursor.execute("CREATE TABLE IF NOT EXISTS `member` (mem_id INTEGER NOT NULL  PRIMARY KEY AUTOINCREMENT, firstname TEXT, middlename TEXT,lastname TEXT, gender TEXT, age TEXT, address TEXT, contact TEXT)")
    cursor.execute("SELECT * FROM `member` ORDER BY `lastname` ASC")
    fetch = cursor.fetchall()
    for data in fetch:
        tree.insert('', 'end', values=(data))
    cursor.close()
    conn.close()
 
def SubmitData():
#    if  FIRSTNAME.get() == "" or LASTNAME.get() == "" or GENDER.get() == "" or AGE.get() == "" or ADDRESS.get() == "" or CONTACT.get() == "":
#            result = tkMessageBox.showwarning('', 'Please Complete The Required Field', icon="warning")
#   else:
        tree.delete(*tree.get_children())
        conn = sqlite3.connect("pythontut.db")
        cursor = conn.cursor()
        cursor.execute("INSERT INTO `member` ( firstname, middlename, lastname, gender, age, address, contact) VALUES(?, ?, ?, ?, ?, ?, ?)", (str(FIRSTNAME.get()), str(MIDDLENAME.get()), str(LASTNAME.get()), str(GENDER.get()), int(AGE.get()), str(ADDRESS.get()), str(CONTACT.get())))
        conn.commit()
        cursor.execute("SELECT * FROM `member` ORDER BY `lastname` ASC")
        fetch = cursor.fetchall()
        for data in fetch:
            tree.insert('', 'end', values=(data))
        cursor.close()
        conn.close()
        FIRSTNAME.set("")
        MIDDLENAME.set("")
        LASTNAME.set("")
        GENDER.set("")
        AGE.set("")
        ADDRESS.set("")
        CONTACT.set("")
 
def UpdateData():
    if GENDER.get() == "":
       result = tkMessageBox.showwarning('', 'Please Complete The Required Field', icon="warning")
    else:
        tree.delete(*tree.get_children())
        conn = sqlite3.connect("pythontut.db")
        cursor = conn.cursor()
        cursor.execute("UPDATE `member` SET `firstname` = ?, `middlename` = ?, `lastname` = ?, `gender` =?, `age` = ?,  `address` = ?, `contact` = ? WHERE `mem_id` = ?", (str(FIRSTNAME.get()), str(MIDDLENAME.get()), str(LASTNAME.get()), str(GENDER.get()), str(AGE.get()), str(ADDRESS.get()), str(CONTACT.get()), int(mem_id)))
        conn.commit()
        cursor.execute("SELECT * FROM `member` ORDER BY `lastname` ASC")
        fetch = cursor.fetchall()
        for data in fetch:
            tree.insert('', 'end', values=(data))
        cursor.close()
        conn.close()
        FIRSTNAME.set("")
        MIDDLENAME.set("")
        LASTNAME.set("")
        GENDER.set("")
        AGE.set("")
        ADDRESS.set("")
        CONTACT.set("")
        
    
def OnSelected(event):
    global mem_id, UpdateWindow
    curItem = tree.focus()
    contents =(tree.item(curItem))
    selecteditem = contents['values']
    mem_id = selecteditem[0]
    FIRSTNAME.set("")
    MIDDLENAME.set("")
    LASTNAME.set("")
    GENDER.set("")
    AGE.set("")
    ADDRESS.set("")
    CONTACT.set("")
    # 1 fn, 2 middle, 3 lastname, 4 gender, 5 age, 6 address, 7 contact
    FIRSTNAME.set(selecteditem[1])
    MIDDLENAME.set(selecteditem[2])
    LASTNAME.set(selecteditem[3])
    AGE.set(selecteditem[5])
    ADDRESS.set(selecteditem[6])
    CONTACT.set(selecteditem[7])
    UpdateWindow = Toplevel()
    UpdateWindow.title("Customer Management System")
    width = 400
    height = 300
    screen_width = root.winfo_screenwidth()
    screen_height = root.winfo_screenheight()
    x = ((screen_width/2) + 450) - (width/2)
    y = ((screen_height/2) + 20) - (height/2)
    UpdateWindow.resizable(0, 0)
    UpdateWindow.geometry("%dx%d+%d+%d" % (width, height, x, y))
    if 'NewWindow' in globals():
        NewWindow.destroy()
 
    #===================FRAMES==============================
    FormTitle = Frame(UpdateWindow)
    FormTitle.pack(side=TOP)
    ContactForm = Frame(UpdateWindow)
    ContactForm.pack(side=TOP, pady=10)
    RadioGroup = Frame(ContactForm)
    Male = Radiobutton(RadioGroup, text="Male", variable=GENDER, value="Male",  font=('arial', 14)).pack(side=LEFT)
    Female = Radiobutton(RadioGroup, text="Female", variable=GENDER, value="Female",  font=('arial', 14)).pack(side=LEFT)
    
    #===================LABELS==============================
    lbl_title = Label(FormTitle, text="Updating Contacts", font=('arial', 16), bg="orange",  width = 300)
    lbl_title.pack(fill=X)
    lbl_firstname = Label(ContactForm, text="Firstname", font=('arial', 14), bd=5)
    lbl_firstname.grid(row=0, sticky=W)
    lbl_middlename = Label(ContactForm, text="Middlename", font=('arial', 14), bd=5)
    lbl_middlename.grid(row=1, sticky=W)
    lbl_lastname = Label(ContactForm, text="Lastname", font=('arial', 14), bd=5)
    lbl_lastname.grid(row=2, sticky=W)
    lbl_gender = Label(ContactForm, text="Gender", font=('arial', 14), bd=5)
    lbl_gender.grid(row=3, sticky=W)
    lbl_age = Label(ContactForm, text="Age", font=('arial', 14), bd=5)
    lbl_age.grid(row=4, sticky=W)
    lbl_address = Label(ContactForm, text="Address", font=('arial', 14), bd=5)
    lbl_address.grid(row=5, sticky=W)
    lbl_contact = Label(ContactForm, text="Contact", font=('arial', 14), bd=5)
    lbl_contact.grid(row=6, sticky=W)
 
    #===================ENTRY===============================
    firstname = Entry(ContactForm, textvariable=FIRSTNAME, font=('arial', 14))
    firstname.grid(row=0, column=1)
    middlename = Entry(ContactForm, textvariable=MIDDLENAME, font=('arial', 14))
    middlename.grid(row=1, column=1)
    lastname = Entry(ContactForm, textvariable=LASTNAME, font=('arial', 14))
    lastname.grid(row=2, column=1)
    RadioGroup.grid(row=3, column=1)
    age = Entry(ContactForm, textvariable=AGE,  font=('arial', 14))
    age.grid(row=4, column=1)
    address = Entry(ContactForm, textvariable=ADDRESS,  font=('arial', 14))
    address.grid(row=5, column=1)
    contact = Entry(ContactForm, textvariable=CONTACT,  font=('arial', 14))
    contact.grid(row=6, column=1)
    
 
    #==================BUTTONS==============================
    btn_updatecon = Button(ContactForm, text="Update", width=50, command=UpdateData)
    btn_updatecon.grid(row=6, columnspan=2, pady=10)
 
 
#fn1353p    
def DeleteData():
    if not tree.selection():
       result = tkMessageBox.showwarning('', 'Please Select Something First!', icon="warning")
    else:
        result = tkMessageBox.askquestion('', 'Are you sure you want to delete this record?', icon="warning")
        if result == 'yes':
            curItem = tree.focus()
            contents =(tree.item(curItem))
            selecteditem = contents['values']
            tree.delete(curItem)
            conn = sqlite3.connect("pythontut.db")
            cursor = conn.cursor()
            cursor.execute("DELETE FROM `member` WHERE `mem_id` = %d" % selecteditem[0])
            conn.commit()
            cursor.close()
            conn.close()
    
def AddNewWindow():
    global NewWindow
    FIRSTNAME.set("")
    MIDDLENAME.set("")
    LASTNAME.set("")
    GENDER.set("")
    AGE.set("")
    ADDRESS.set("")
    CONTACT.set("")
    NewWindow = Toplevel()
    NewWindow.title("Customer Management System")
    width = 400
    height = 300
    screen_width = root.winfo_screenwidth()
    screen_height = root.winfo_screenheight()
    x = ((screen_width/2) - 455) - (width/2)
    y = ((screen_height/2) + 20) - (height/2)
    NewWindow.resizable(0, 0)
    NewWindow.geometry("%dx%d+%d+%d" % (width, height, x, y))
    if 'UpdateWindow' in globals():
        UpdateWindow.destroy()
    
    #===================FRAMES==============================
    FormTitle = Frame(NewWindow)
    FormTitle.pack(side=TOP)
    ContactForm = Frame(NewWindow)
    ContactForm.pack(side=TOP, pady=10)
    RadioGroup = Frame(ContactForm)
    Male = Radiobutton(RadioGroup, text="Male", variable=GENDER, value="Male",  font=('arial', 14)).pack(side=LEFT)
    Female = Radiobutton(RadioGroup, text="Female", variable=GENDER, value="Female",  font=('arial', 14)).pack(side=LEFT)
    
    #===================LABELS==============================

#ADDED
    lbl_title = Label(FormTitle, text="Adding New Customer", font=('arial', 16), bg="#66ff66",  width = 300)
    lbl_title.pack(fill=X)
    lbl_firstname = Label(ContactForm, text="Firstname", font=('arial', 14), bd=5)
    lbl_firstname.grid(row=0, sticky=W)
    lbl_middlename = Label(ContactForm, text="Middlename", font=('arial', 14), bd=5)
    lbl_middlename.grid(row=1, sticky=W)
    lbl_lastname = Label(ContactForm, text="Lastname", font=('arial', 14), bd=5)
    lbl_lastname.grid(row=2, sticky=W)
    lbl_gender = Label(ContactForm, text="Gender", font=('arial', 14), bd=5)
    lbl_gender.grid(row=3, sticky=W)
    lbl_age = Label(ContactForm, text="Age", font=('arial', 14), bd=5)
    lbl_age.grid(row=4, sticky=W)
    lbl_address = Label(ContactForm, text="Address", font=('arial', 14), bd=5)
    lbl_address.grid(row=5, sticky=W)
    lbl_contact = Label(ContactForm, text="Contact", font=('arial', 14), bd=5)
    lbl_contact.grid(row=6, sticky=W)



    #===================ENTRY===============================

#ADDED
    firstname = Entry(ContactForm, textvariable=FIRSTNAME, font=('arial', 14))
    firstname.grid(row=0, column=1)
    middlename = Entry(ContactForm, textvariable=MIDDLENAME, font=('arial', 14))
    middlename.grid(row=1, column=1)
    lastname = Entry(ContactForm, textvariable=LASTNAME, font=('arial', 14))
    lastname.grid(row=2, column=1)
    RadioGroup.grid(row=3, column=1)
    age = Entry(ContactForm, textvariable=AGE,  font=('arial', 14))
    age.grid(row=4, column=1)
    address = Entry(ContactForm, textvariable=ADDRESS,  font=('arial', 14))
    address.grid(row=5, column=1)
    contact = Entry(ContactForm, textvariable=CONTACT,  font=('arial', 14))
    contact.grid(row=6, column=1)

 
    #==================BUTTONS==============================
    btn_addcon = Button(ContactForm, text="Save", width=50, command=SubmitData)
    btn_addcon.grid(row=6, columnspan=2, pady=10)
 
 
 #==================================== MAIN LOOP ================================


#main():
result = str
root = Tk()
root.title("Customer Management System")
width = 700
height = 400
screen_width = root.winfo_screenwidth()
screen_height = root.winfo_screenheight()
x = (screen_width/2) - (width/2)
y = (screen_height/2) - (height/2)
root.geometry("%dx%d+%d+%d" % (width, height, x, y))
root.resizable(0, 0)
root.config(bg="black")
#============================VARIABLES===================================
FIRSTNAME = StringVar()
MIDDLENAME = StringVar()
LASTNAME = StringVar()
GENDER = StringVar()
AGE = StringVar()
ADDRESS = StringVar()
CONTACT = StringVar() 
#============================FRAMES======================================
Top = Frame(root, width=500, bd=1, relief=SOLID)
Top.pack(side=TOP)
Mid = Frame(root, width=500,  bg="black")
Mid.pack(side=TOP)
MidLeft = Frame(Mid, width=100)
MidLeft.pack(side=LEFT, pady=10)
MidLeftPadding = Frame(Mid, width=370, bg="black")
MidLeftPadding.pack(side=LEFT)
MidRight = Frame(Mid, width=100)
MidRight.pack(side=RIGHT, pady=10)
TableMargin = Frame(root, width=500)
TableMargin.pack(side=TOP)
#============================LABELS======================================
lbl_title = Label(Top, text="Customer Management System", font=('arial', 16), width=500,bg="blue")
lbl_title.pack(fill=X)
 
#============================ENTRY=======================================
 
#============================BUTTONS=====================================
btn_add = Button(MidLeft, text="+ ADD NEW", bg="#66ff66", command=AddNewWindow)
btn_add.pack()
btn_delete = Button(MidRight, text="DELETE", bg="red", command=DeleteData)
btn_delete.pack(side=RIGHT)
 
#============================TABLES======================================
scrollbarx = Scrollbar(TableMargin, orient=HORIZONTAL)
scrollbary = Scrollbar(TableMargin, orient=VERTICAL)
tree = ttk.Treeview(TableMargin, columns=("MemberID", "Firstname", "Middlename", "Lastname", "Gender", "Age", "Address", "Contact"), height=400, selectmode="extended", yscrollcommand=scrollbary.set, xscrollcommand=scrollbarx.set)
scrollbary.config(command=tree.yview)
scrollbary.pack(side=RIGHT, fill=Y)
scrollbarx.config(command=tree.xview)
scrollbarx.pack(side=BOTTOM, fill=X)
tree.heading('MemberID', text="MemberID", anchor=W)
tree.heading('Firstname', text="Firstname", anchor=W)
tree.heading('Middlename', text="Middlename", anchor=W)
tree.heading('Lastname', text="Lastname", anchor=W)
tree.heading('Gender', text="Gender", anchor=W)
tree.heading('Age', text="Age", anchor=W)
tree.heading('Address', text="Address", anchor=W)
tree.heading('Contact', text="Contact", anchor=W)
tree.column('#0', stretch=NO, minwidth=0, width=0)
tree.column('#1', stretch=NO, minwidth=0, width=0)
tree.column('#2', stretch=NO, minwidth=0, width=80)
tree.column('#3', stretch=NO, minwidth=0, width=120)
tree.column('#4', stretch=NO, minwidth=0, width=90)
tree.column('#5', stretch=NO, minwidth=0, width=80)
tree.column('#6', stretch=NO, minwidth=0, width=120)
tree.column('#7', stretch=NO, minwidth=0, width=120)
tree.pack()
tree.bind('<Double-Button-1>', OnSelected)

#============================INITIALIZATION==============================
if __name__ == '__main__':
    Database()
    root.mainloop()

