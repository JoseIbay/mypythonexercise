
from dataclasses import dataclass
import datetime

@dataclass
class Customer :
    Lastname : str
    Middlename : str
    Firstname : str
    Suffix : str
    Address1 : str
    Address2 : str
    Telephone : str
    Zipcode : str
    State : str 
    Country : str 
    Areacode : str
    Birthdate : datetime.date
    Age : float 
    Gender : str
    UserRoleCode : str 
    ICNumber : str
    
    def __post_init__(self):
        self.ICNumber = "S7484717Z"


class UserTable :
    UserTableUserRoleCode : str
    UserTableUserID : str 
    UserTableCustomercodeclassification : str 
    UserTableCompanycodeclassification : str 
    UserTableCountrycoddeclassification : str  

#Currency All, USD, PHP
#Product All, Loans, Syndication
#Company All, Airbus
#Country All, Singapore, France

class UserMatrix :
    UserMatrixUserID : str 
    UserMatrixCurrencycodeclassification : str 
    UserMatrixProductcodeclassification : str 
    UserMatrixCompanycodeclassification : str 
    UserMatrixCountrycoddeclassification : str  
    UserMatrixBranchcodeclassification : str 
    UserMatrixSubbranchcodeclassification : str  

class Company : 
    CompanyCode : str 
    CompanyName : str 
    CompanyAddress1 : str
    CompanyAddress2 : str
    CompanyState : str
    CompanyZipcode : str
    CompanyTelephone : str
    CompanyContact1FirstName  : str
    CompanyContact1MiddleName : str
    CompanyContact1LastName : str
    CompanyContact1Suffix : str
    CompanyDomicileCountry : str
    CompanyDomicileState : str
    CompanyDomicileCountryCode : str
    CompanyDomicileTelephone : str
    CompanySECRegistration : str 
    CompanyDateofIncorporation : datetime.date
    CompanyCreditRatingFitch : str 
    CompanyCreditRatingOthers : str 
    CompanyTaxclassification : str 
    CompanyDepartment : str 
    CompanyRelationshipManager : str
#aggregate facilities

class FacilitySetup :
    CompanyCode : str 
    FacilityNumber : str 
    FacilityAmount : float
    ProductCode : str 
    CurrencyCode : str
    Currency2nd : str 
    Currency3rd : str 
    FacilityAggregate : str 
    Revolver : str 
    FacilityTotalAmountAggregate : float
    FacilityRemainingAggregate : float 
    FacilityExpirationDate : datetime
    Facilitygraceperiod : float
#transactional facilities
class TransactionTable :
    TransactionCode : str
    ProductCode : str 
    CurrencyCode : str
    TransactionAmount : float
    FacilityNumber : str 
    AccountsPostDate = datetime.date
    AccountsValueDate = datetime.date
    AccountsAvailableDate = datetime.date
    Interestrate : float 
    EndDate : datetime.date
    BuyCCY : float
    BuyAmount : float
    SellCCY : float 
    SellAmount : float 
    FXrate : float 
    swappoints : float 
    BuyCCYIntrate : float
    SellCCYIntrate : float 
    Subbranch : str 
    Branch  :  str 
    Department : str 
    IncomeDept : str 
    BookingDept : str 
    BasisCalculation : str 
    FirstInterestDate : datetime.date
    FirstAccrualShort : str 
    FirstAccrualLong : str 
    TotalNumberofdays : float 
    CouponDateStart : datetime.date
    CouponDateEnd : datetime.date
class ProductTable : 
    ProductCode : str 
    CurrencyCode : str    
    ProductDescription : str 
    Interestrate : float 
    EndDate : datetime.date
    BuyCCY : float
    BuyAmount : float
    SellCCY : float 
    SellAmount : float 
    FXrate : float 
    swappoints : float 
    BuyCCYIntrate : float
    SellCCYIntrate : float 
    BasisCalculation : str 
    FirstInterestDate : datetime.date
    FirstAccrualShort : str 
    FirstAccrualLong : str 
    TotalNumberofdays : float 
    CouponDateStart : datetime.date
    CouponDateEnd : datetime.date

class BalanceTable:
    BalanceSheetSubitem = str 
    BalanceSheetItem = str 
    AccountsID = str
    AccountsType = str 
    AccountsCCY = str
    AccountsRef = str 
    AccountsBranch = str 
    AccountsSubbranch = str 
    AccountsDept = str 
    BalanceAvailableAmount = float 
    BalancePostAmount = float 
    BalanceValueAmount = float 
    AccountsPostDate = datetime.date
    AccountsValueDate = datetime.date
    AccountsAvailableDate = datetime.date
    BanksAcct = str 
class AccountsTable : 
    BalanceSheetSubitem = str 
    BalanceSheetItem = str 
    AccountsID = str
    AccountsType = str 
    AccountsCCY = str
    AccountsRef = str 
    AccountsBranch = str 
    AccountsSubbranch = str 
    AccountsDept = str 
    BanksAcct = str 
    
class JournalTable :
    BalanceSheetSubitem = str 
    BalanceSheetItem = str 
    AccountsID = str
    AccountsType = str 
    AccountsCCY = str
    AccountsRef = str 
    AccountsBranch = str 
    AccountsSubbranch = str 
    AccountsDept = str 
    JournalAmount = float 
    TotalAmount = float 
    AccountsPostDate = datetime.date
    AccountsValueDate = datetime.date
    AccountsAvailableDate = datetime.date
    BanksAcct = str 
    DRCR = str 
    TransactionCode : str

class vehicle:
    carname = "BMW 320i"
    kind = "Car Sedan"
    color = "blue"
    value = 234000

    def desc(self):
        desc_str = "%s is a %s %s worth $%.2f." % (self.carname, self.color, self.kind, self.value)
        return desc_str


car1 = vehicle()
car1.carname = "Ferrari"
car1.color = "red"
car1.value = 450000.00
car1.kind = "convertible"

car2 = vehicle()
car2.carname = "Volkswagen"
car2.color = "white"
car2.value = 360000.00
car2.kind = "SUV"

print(car1.desc())
print(car2.desc())


#print(car2.description)




#Create some instances
#MyCustomerrecord = Customer ()
MyCustomerrecord = Customer (
    "Ibay"
    ,"Diestro"
    ,"Jose"
    ,"Jr"
    ,"13 Holland Drive"
    ,"Unit #10-40"
    ,"94747674"
    ,"271013"
    ,"Singapore"
    ,"Singapore"
    ,"65"
    ,"19740706"
    ,47
    ,"Male"
    ,"Lending01"
    ,"S88747464")

print(MyCustomerrecord)
print ("%s, %s %s %s can be reached at +%s-%s" % (MyCustomerrecord.Lastname, MyCustomerrecord.Firstname, MyCustomerrecord.Suffix, MyCustomerrecord.Middlename, MyCustomerrecord.Areacode, MyCustomerrecord.Telephone))
print("My birthday is on %s" % (MyCustomerrecord.Birthdate ))


import calendar


yy = 2022
mm = 3


jay = calendar.day_name

print(calendar.month(yy,mm))
print(jay)


mike = []
mike.append("123")
mike.append("678")

if mike[1] == "678" :
    print("Mike is " , mike[0])
else:
    print("Nope it's not 123")

n = 0
for n in range(0,len(mike)):
    print(mike[n])

        
