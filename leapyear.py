
from operator import truediv

#end of months stores a list, with reference to index starting with zero, then 1,2,3, etc. 1 = January, 2 =Feb
end_of_months = [0,31,28,31,30,31,30,31,31,30,31,30,31]

#define a funcction that when called, will calculate the year divided by 4 or 400. the % means that if the remainder is zero 
# it will return as true - thereby identifying that the year is a leap year.
def is_it_leap(y):
    if (y % 4 == 0 ) or (y % 400 == 0 ) :
        return "Yes"


year  = input('What is the year ?')
month = input('What is the month ?')
# call the function, and pass 2021 as the year
if is_it_leap(year) == "Yes" and int(month) == 2:
    print ('The year is {}, and the month is {}. So the month-end day is {}'.format(year, month, 29) 
else:
    print ('The year is {}, and the month is {}. So the month-end day is {}'.format(year, month, end_of_months[month])

