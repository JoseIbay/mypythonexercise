
#from black import pyc_attr
from email.utils import decode_rfc2231
import pandas as pd
import numpy as np
#import sqlite3 

def ProductImport():
    n=0
    global MyProduct
    MyProduct = {}
    global MyPrice
    MyPrice = {}
    global MyQty 
    MyQty = {}
    global df1
    df1 = pd.read_csv('/Users/joseibay/documents/mypythonexercise/product_data.csv')
#get column names
    for n in range(0,len(df1)):
        
    #print(df.Product_No[n])
    #print(df.head(1))
    #MyProduct.update({"'Product_No'",':',"'",df.Product_No[n].strip(),"'"})
        MyProduct.update({ df1.Product_No[n]: df1.ProductName[n]})
        MyPrice.update({ df1.Product_No[n] : df1.Price[n]})                  
        MyQty.update({df1.Product_No[n] : df1.ProductQty[n]})     
        print("{'Product_No'",":'",df1.Product_No[n],"'",",")
        print("'Product_Price'",":",df1.Price[n],"'",",")
        print("'Product_Qty'",":",df1.ProductQty[n],"'}")
    #tempconcatenate1 = "{'Product_No'",":'",df.Product_No[n],"'",","
    #tempconcatenate2 = "'Product_Price'",":",df.Price[n],"'",","
    #tempconcatenate3 = "'Product_Qty'",":",df.ProductQty[n],"'}"
    #print(MyQty['P12345678XX'])  #will give the quantity of the product
    #print(MyPrice['P12345678XX']) #will give the price of the product   
    #df1.to_csv ('/Users/joseibay/documents/mypythonexercise/productexport.csv')          


def CustomerImport():
    global df2
    df2 = pd.read_csv('/Users/joseibay/documents/mypythonexercise/customer_data.csv')
    n=0
    global MyCustomer 
    MyCustomer = {}
    global MyCustomerAge 
    MyCustomerAge = {}
    global MyCustomerAddress 
    MyCustomerAddress = {}
    global MyCustomerGender 
    MyCustomerGender = {}
    for n in range(0,len(df2)):
        MyCustomer.update({df2.Customer_No[n] : df2.LastName[n]})
        MyCustomerAge.update({df2.Customer_No[n] : df2.Age[n]})
        MyCustomerAddress.update({df2.Customer_No[n] : df2.Address[n]})
        MyCustomerGender.update({df2.Customer_No[n] : df2.Gender[n]})
    #df2.to_csv('Customerexport.csv')  
    #print(MyCustomer['12345'])
    #print(MyCustomerAge['12345'])
    #print(MyCustomerAddress['12345'])
    #print(MyCustomerGender['12345'])

def SalesImport():
    n = 0
    global MySalesInvoice
    global MySalesCustomer
    global MySalesQty
    global MySalesAmount
    global MySalesdate
    global MyUserNo
    global df3 
    MySalesInvoice = {}
    MySalesCustomer = {}
    MySalesQty = {}
    MySalesAmount = {}
    MySalesdate = {}
    MyUserNo = {}
    df3 = pd.read_csv('Sales_data.csv')
    for n in range(0,len(df3)):
        MySalesInvoice.update({df3.Sales_Invoice[n] : df3.Product_No[n]})
        MySalesCustomer.update({df3.Sales_Invoice[n] : df3.Customer_No[n]})
        MySalesQty.update({df3.Sales_Invoice[n] : df3.Quantity[n]})
        MySalesAmount.update({df3.Sales_Invoice[n] : df3.Amount[n]})
        MySalesdate.update({df3.Sales_Invoice[n] : df3.DateofPurchase[n]})
        MyUserNo.update({df3.Sales_Invoice[n] : df3.User_No[n]})
    #df3.to_csv('salesexport.csv')
        


if __name__ == '__main__':
    ProductImport()
    CustomerImport()
    SalesImport()
#    print(df1.Product_No[1])
#    print(MyProduct[df1.Product_No[1]])
#    print(MyPrice["P12345678XX"])
#    print(df2.Customer_No[1])
#    CustomerNumber = str(MySalesCustomer["1000000001A"])
#    print(MyCustomer[CustomerNumber] )
#    print(MyCustomerAddress[CustomerNumber])
    #print(df1)
    #print(df2)
    #print(df3)#
    #print(df3.dtypes)
    #print(df1.Product_No[0])
    #print(df3.Product_No[0])  
    #print(df2.Customer_No[0])
    #print(df3.Customer_No[0])
    n,x = 0,0
    #print(str(df2.Customer_No[0]) )
    #keystring = "'" + str(df2.Customer_No[0]) + "'"
    #print (keystring) #'12345'
    #print(MySalesCustomer[df3.Sales_Invoice[0]]) #A33385
    #print(MyCustomer[MySalesCustomer[df3.Sales_Invoice[0]]]) #DE VERA
    
    #print(str(MySalesCustomer[df3.Sales_Invoice[0]]) )
    #print( str(df2.Customer_No[0]))
    
    for n in range(n,len(df3)):
        if MySalesCustomer[df3.Sales_Invoice[n]]  == df2.Customer_No[n]:
            print('went in')
            df3.insert(4,'CustomerName',MyCustomer[df2.Customer_No[n]])
    #print(df3)
    
    global df4
    df4 = {}
    df4 = pd.DataFrame({
    "FirstName":["Kelvin","Joe","Jay","Kyla","Paolo","Anton","Elmo","Tony","Pey","Emer","Cynthia","Froylan","Bryan","Andrew","Christophe","Evangeline","Dancing","River","Phoenix","Jose","Bon","Bonita","John","Julia","Roberts"],
    "MiddleName":["Kelvin","Joe","Jay","Diestro","Udjang","Udjang","Udjang","Udjang","Udjang","Udjang","Udjang","Udjang","Udjang","Udjang","Evangeline","Dancing","River","Phoenix","John","Diestro","Gie","Gie","Roberts","Julia","Christophe"],
    "LastName":["Kelvin","Chan","Laurel","Park","Hong","Goldstein","Nevada","Virata","Ibay","Diestro","Ayala","Tioseco","Manuba","Garcia","James","Nickee","Keller","Santos","Paulo","Ibay","De Vera","De Vera","David","Saul","Paul"],
    "Age":[32,35,30,30,23,23,22,21,45,78,66,55,44,43,34,23,34,33,44,47,47,47,34,45,45],
    "Address":["13 Holand Drive","13 Holand Drive","13 Holand Drive","13 Holand Drive","13 Holand Drive","13 Holand Drive","13 Holand Drive","13 Holand Drive","13 Holand Drive","13 Holand Drive","13 Holand Drive","13 Holand Drive","13 Holand Drive","13 Holand Drive","18 Holand Drive","19 Holand Drive","20 Holand Drive","21 Holand Drive","22 Holand Drive","13 Holland Drive #10-40 Singapore","65 Belmond Road Singapore","65 Belmond Road Singapore","15 Holand Drive","13 Holand Drive","17 Holand Drive"],
    "Gender":["Male","Male","Male","Female","Male","Male","Male","Male","Female","Female","Female","Male","Male","Male","Male","Female","Male","Male","Male","Male","Male","Male","Male","Female","Male"],
    "Customer_No":["12345","12346","12347","12348","12349","12350","12351","12352","12353","12354","12355","12356","12357","12358","A333123","A333124","A333125","A333126","A333127","A33383","A33384","A33385","A33386","A33387","A33388"]
    })
    n = 0 
    #print (df4.Age[3])
    df4.to_numpy()
    arr = np.array({
    "FirstName":["Kelvin","Joe","Jay","Kyla","Paolo","Anton","Elmo","Tony","Pey","Emer","Cynthia","Froylan","Bryan","Andrew","Christophe","Evangeline","Dancing","River","Phoenix","Jose","Bon","Bonita","John","Julia","Roberts"],
    "MiddleName":["Kelvin","Joe","Jay","Diestro","Udjang","Udjang","Udjang","Udjang","Udjang","Udjang","Udjang","Udjang","Udjang","Udjang","Evangeline","Dancing","River","Phoenix","John","Diestro","Gie","Gie","Roberts","Julia","Christophe"],
    "LastName":["Kelvin","Chan","Laurel","Park","Hong","Goldstein","Nevada","Virata","Ibay","Diestro","Ayala","Tioseco","Manuba","Garcia","James","Nickee","Keller","Santos","Paulo","Ibay","De Vera","De Vera","David","Saul","Paul"],
    "Age":[32,35,30,30,23,23,22,21,45,78,66,55,44,43,34,23,34,33,44,47,47,47,34,45,45],
    "Address":["13 Holand Drive","13 Holand Drive","13 Holand Drive","13 Holand Drive","13 Holand Drive","13 Holand Drive","13 Holand Drive","13 Holand Drive","13 Holand Drive","13 Holand Drive","13 Holand Drive","13 Holand Drive","13 Holand Drive","13 Holand Drive","18 Holand Drive","19 Holand Drive","20 Holand Drive","21 Holand Drive","22 Holand Drive","13 Holland Drive #10-40 Singapore","65 Belmond Road Singapore","65 Belmond Road Singapore","15 Holand Drive","13 Holand Drive","17 Holand Drive"],
    "Gender":["Male","Male","Male","Female","Male","Male","Male","Male","Female","Female","Female","Male","Male","Male","Male","Female","Male","Male","Male","Male","Male","Male","Male","Female","Male"],
    "Customer_No":["12345","12346","12347","12348","12349","12350","12351","12352","12353","12354","12355","12356","12357","12358","A333123","A333124","A333125","A333126","A333127","A33383","A33384","A33385","A33386","A33387","A33388"]
    })
    
 

      
#notes              
#columnnames.append(df.columns)    
#print(df.Product_No[4])
#print(MyProd#uct[df.Product_No[4]])
#print(MyPrice[df.Product_No[4]])
#P12345678BB
#P12345678CC
#P12345678DD
#P12345678EE
